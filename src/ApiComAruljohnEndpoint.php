<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-aruljohn-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComAruljohn;

use ArrayIterator;
use InvalidArgumentException;
use Iterator;
use PhpExtended\Html\HtmlCollectionNodeInterface;
use PhpExtended\Html\HtmlParser;
use PhpExtended\Html\HtmlParserInterface;
use PhpExtended\Html\HtmlTextNode;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Parser\ParseThrowable;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use RuntimeException;

/**
 * UserAgentArulJohnEndpoint class file.
 * 
 * This class is the interpreter of the data at the https://aruljohn.com url.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiComAruljohnEndpoint implements ApiComAruljohnEndpointInterface
{
	
	/**
	 * The http client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 * 
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 * 
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The html parser.
	 * 
	 * @var HtmlParserInterface
	 */
	protected HtmlParserInterface $_htmlParser;
	
	/**
	 * Builds a new Endpoint with its dependancies.
	 * 
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?HtmlParserInterface $htmlParser
	 */
	public function __construct(
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?HtmlParserInterface $htmlParser = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_htmlParser = $htmlParser ?? new HtmlParser();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiComAruljohn\ApiComAruljohnEndpointInterface::getUserAgentIterator()
	 */
	public function getUserAgentIterator() : Iterator
	{
		try
		{
			$uri = $this->_uriFactory->createUri('https://aruljohn.com/ua/');
		}
		catch(InvalidArgumentException $exc)
		{
			throw new RuntimeException('Failed to parse target url', -1, $exc);
		}
		
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		
		try
		{
			/** @var HtmlCollectionNodeInterface $dom */
			$dom = $this->_htmlParser->parse($response->getBody()->__toString());
		}
		catch(ParseThrowable $e)
		{
			$message = 'Failed to parse body for url {url}';
			$context = ['{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		try
		{
			$data = $dom->findNodeCss('div[style="max-width:800px"]');
		}
		catch(ParseThrowable $exc)
		{
			throw new RuntimeException('Failed to parse css selector', -1, $exc);
		}
		
		if(null === $data || !($data instanceof HtmlCollectionNodeInterface))
		{
			// @codeCoverageIgnoreStart
			$message = 'Failed to find the body of the data for url {url}';
			$context = ['{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		$userAgents = [];
		
		foreach($data as $datum)
		{
			if($datum instanceof HtmlTextNode)
			{
				$userAgent = \trim(\trim($datum->getText()), '-');
				if(empty($userAgent) || \strpos($userAgent, '800px') !== false)
				{
					continue;
				}
				
				$userAgents[] = $userAgent;
			}
		}
		
		return new ArrayIterator($userAgents);
	}
	
}
