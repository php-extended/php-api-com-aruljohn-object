# php-extended/php-api-com-aruljohn-object

A php api wrapper around the aruljohn user agent string database.

![coverage](https://gitlab.com/php-extended/php-api-com-aruljohn-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-com-aruljohn-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-com-aruljohn-object ^8`


## Basic Usage

You may use this library the following way :

```php

use PhpExtended\AruljohnComApi\ArulJohnComApiEndpoint;
use PhpExtended\Endpoint\HttpEndpoint;

/* @var $client \Psr\Http\Client\ClientInterface */

$endpoint = new ArulJohnComApiEndpoint(new HttpEndpoint($client));

$userAgents = $endpoint->getUserAgentIterator();
// returns \Iterator<string> the list of user agent values

```


## License

MIT (See [license file](LICENSE)).
