<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-aruljohn-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiComAruljohn\ApiComAruljohnEndpoint;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\StringStream;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * ApiComAruljohnEndpointTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiComAruljohn\ApiComAruljohnEndpoint
 *
 * @internal
 *
 * @small
 */
class ApiComAruljohnEndpointTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiComAruljohnEndpoint
	 */
	protected ApiComAruljohnEndpoint $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$k = 0;
		
		foreach($this->_object->getUserAgentIterator() as $userAgent)
		{
			$this->assertNotEmpty($userAgent);
			$k++;
		}
		
		$this->assertGreaterThan(1, $k);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface
		{
			
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				$options = ['http' => ['user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4398.0']];
				$data = \file_get_contents($request->getUri()->__toString(), false, \stream_context_create($options));
				$body = new StringStream($data);
				
				return (new Response())->withBody($body);
			}
		};
		
		$this->_object = new ApiComAruljohnEndpoint($client);
	}
	
}
